import cv2
import numpy as np

# URL = "http://192.168.2.83:8081/"
URL = "http://access.aimtechackathon.cz:8083/"

cap = cv2.VideoCapture()
cap.open(URL)


def get_top_image():
    rval, frame = cap.read()
    if not rval:
        print("reconnecting")
        cap.release()
        cap.open(URL)
        frame = get_top_image()
    return frame


def getCenter(corners):
    cX = 0
    cY = 0

    for corner in corners:
        cX += corner[1]
        cY += corner[0]

    return int(cY / 4), int(cX // 4)

    # (topLeft, topRight, bottomRight, bottomLeft) = corners
    # topRight = (int(topRight[0]), int(topRight[1]))
    # bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
    # bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
    # topLeft = (int(topLeft[0]), int(topLeft[1]))
    # cY = int((topLeft[0] + bottomRight[0]) / 2.0)
    # cX = int((topLeft[1] + bottomRight[1]) / 2.0)
    #
    # return cX, cY


leftDown, rightTop = None, None
def get_top_properties(image, ddd=True):
    global leftDown, rightTop
    arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
    arucoParams = cv2.aruco.DetectorParameters_create()
    (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)

    for i, id in enumerate(ids):
        (topLeft, topRight, bottomRight, bottomLeft) = corners[i][0]
        if id == 31:
            leftDown = (int(bottomLeft[0]), int(bottomLeft[1]))
        elif id == 33:
            rightTop = (int(topRight[0]), int(topRight[1]))

    if not leftDown or not rightTop: return None, None, None, None
    newImg = image[rightTop[1] - 10:leftDown[1] + 10, leftDown[0] - 20:rightTop[0] + 40]
    (corners, ids, rejected) = cv2.aruco.detectMarkers(newImg, arucoDict, parameters=arucoParams)

    if ddd:
        cv2.aruco.drawDetectedMarkers(newImg, corners, ids)

    ids = np.array([]) if ids is None else ids
    black, green, robot = [[None, None], [None, None], [None, None, None]]
    for i, id in enumerate(ids):
        (topLeft, topRight, bottomRight, bottomLeft) = corners[i][0]
        if id == 311:
            black[0] = (int(bottomLeft[0]), int(bottomLeft[1]))
        elif id == 313:
            black[1] = (int(topRight[0]), int(topRight[1]))

        elif id == 321:
            green[0] = (int(bottomLeft[0]), int(bottomLeft[1]))
        elif id == 323:
            green[1] = (int(topRight[0]), int(topRight[1]))

        elif id == 3:
            corn = corners[i][0]
            center = getCenter(corn)
            robot[0] = center[0]
            robot[1] = center[1]

            middle_forward_point_X = (int(corn[2][0]) + int(corn[3][0])) // 2
            middle_forward_point_Y = (int(corn[2][1]) + int(corn[3][1])) // 2
            cv2.circle(newImg, (int(middle_forward_point_X), int(middle_forward_point_Y)), color=(0, 255, 0), radius=4)


            robot[2] = (middle_forward_point_X, middle_forward_point_Y)

    if black[0] is None or black[1] is None or green[0] is None or green[1] is None or robot[0] is None or robot[1] is None or robot[2] is None:
        return None, None, None, None

    return newImg, black, green, robot


def get_top_camera_all_important(ddd=True):
    newImg, black, green, robot = None, None, None, None
    while newImg is None:
        image = get_top_image()
        newImg, black, green, robot = get_top_properties(image, ddd)

    return newImg, black, green, robot


if __name__ == '__main__':
    while True:
        image = get_top_image()
        cv2.imshow("top", image)
        cv2.waitKey(50)

    # from imageai.Detection.Custom import CustomObjectDetection
    #
    # arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
    # arucoParams = cv2.aruco.DetectorParameters_create()
    #
    # detector = CustomObjectDetection()
    # detector.setModelTypeAsYOLOv3()
    # detector.setModelPath("detection_model-ex-027--loss-0016.744.h5")
    # detector.setJsonPath("detection_config.json")
    # detector.loadModel()
    # while True:
    #     print("Start")
    #     newImg, black, green, robot = get_top_camera_all_important(ddd=True)
    #
    #     cv2.rectangle(newImg, black[0], black[1], (50, 50, 50), 3)
    #     cv2.rectangle(newImg, green[0], green[1], (0, 128, 0), 3)
    #     cv2.circle(newImg, (robot[0], robot[1]), 8, (0, 0, 255), -1)
    #     # cv2.arrowedLine(newImg, (robot[0]), (robot[1]))  # TODO
    #
    #     cv2.imwrite("krava.png", newImg)
    #
    #     detections = detector.detectObjectsFromImage(input_image="krava.png", output_image_path="krava-new.png", minimum_percentage_probability=30)
    #     for detection in detections:
    #         points = detection["box_points"]
    #         print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
    #         print("--------------------------------")
    #
    #         cv2.rectangle(newImg, [points[0], points[1]], [points[2], points[3]], (0, 0, 255), 3)
    #
    #         cv2.imshow("newImg", newImg)
    #         key = cv2.waitKey(20)

