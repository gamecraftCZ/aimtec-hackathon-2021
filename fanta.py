import cv2
from get_front_image import get_front_image


def getRotDist(corners):
    j = (corners[2][1] - corners[1][1])
    dist = 1000 / j
    r = corners[1][0] - corners[0][0]
    rot = j / r
    return rot, dist


if __name__ == '__main__':
    # image = cv2.imread("fanta.png")
    while True:
        image = get_front_image()
        arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
        arucoParams = cv2.aruco.DetectorParameters_create()

        (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
        targetId = None
        targetCorners = None
        for i, id in enumerate(ids.flatten()):
            targetId = id
            targetCorners = corners[i][0]
            break

        ######################
        cv2.aruco.drawDetectedMarkers(image, corners, ids)
        cv2.imshow("image", image)
        cv2.waitKey(20)

        if not targetId:
            print("No target id!")
            continue

        rot, dist = getRotDist(targetCorners)
        print(rot, dist)

