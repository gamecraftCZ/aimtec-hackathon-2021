from imageai.Detection.Custom import CustomObjectDetection

detector = CustomObjectDetection()
detector.setModelTypeAsYOLOv3()
detector.setModelPath("detection_model-ex-072--loss-0026.789.h5")
detector.setJsonPath("detection_config.json")
detector.loadModel()

detections = detector.detectObjectsFromImage(input_image="vejce.png", output_image_path="vejce-new.png", minimum_percentage_probability=30)
for detection in detections:
    print(detection["name"], " : ", detection["percentage_probability"], " : ", detection["box_points"])
    print("--------------------------------")
