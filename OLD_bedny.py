import cv2
from numpy import array as a
import numpy as np
from top_camera import get_top_image


def maskBedny(img: np.ndarray) -> np.ndarray:
    hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV) # Convert to hsv
    masked = cv2.inRange(hsv, a([0, 30, 60]), a([90, 128, 200])) # mask
    return masked


img = get_top_image()
# cv2.imshow("img", img)
# key = cv2.waitKey()

# img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

# TODO divný tag


# hsv = cv2.cvtColor(img, cv2.COLOR_RGB2HSV)
# cv2.imshow("hsv", hsv)
# key = cv2.waitKey()

mask = maskBedny(img)
cv2.imshow("mask", mask); key = cv2.waitKey()

# #635350
