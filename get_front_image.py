import picamera
from time import sleep
from io import BytesIO
from time import sleep
from picamera import PiCamera
from PIL import Image
import numpy as np


# Create the in-memory stream
camera = PiCamera()

camera.preview_fullscreen = False
camera.preview_window = (200, 50, 640, 480)

camera.start_preview()

def get_front_image():
    stream = BytesIO()

    camera.capture(stream, format='jpeg')
    # "Rewind" the stream to the beginning so we can read its content
    stream.seek(0)
    image = Image.open(stream)

    img = np.array(image)
    return img
