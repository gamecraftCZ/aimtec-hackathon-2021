import cv2
# from numpy import array as a
# import numpy as np
# from top_camera import get_top_image

image = cv2.imread("jahoda.png")
# image = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
# cv2.imshow("img", image); key = cv2.waitKey()


im = image.copy()
img = cv2.cvtColor(im, cv2.COLOR_RGB2GRAY)

arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
arucoParams = cv2.aruco.DetectorParameters_create()
(corners, ids, rejected) = cv2.aruco.detectMarkers(img, arucoDict, parameters=arucoParams)

cv2.aruco.drawDetectedMarkers(im, corners, ids)
# cv2.aruco.drawDetectedMarkers(im, rejected, borderColor=(100,0, 240))

cv2.imshow("Image", im)
cv2.waitKey(0)

# verify *at least* one ArUco marker was detected
# if len(corners) > 0:
# 	# flatten the ArUco IDs list
# 	ids = ids.flatten()
# 	# loop over the detected ArUCo corners
# 	for (markerCorner, markerID) in zip(corners, ids):
# 		# extract the marker corners (which are always returned in
# 		# top-left, top-right, bottom-right, and bottom-left order)
# 		corners = markerCorner.reshape((4, 2))
# 		(topLeft, topRight, bottomRight, bottomLeft) = corners
# 		# convert each of the (x, y)-coordinate pairs to integers
# 		topRight = (int(topRight[0]), int(topRight[1]))
# 		bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
# 		bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
# 		topLeft = (int(topLeft[0]), int(topLeft[1]))
#
# 		# draw the bounding box of the ArUCo detection
# 		cv2.line(image, topLeft, topRight, (0, 255, 0), 2)
# 		cv2.line(image, topRight, bottomRight, (0, 255, 0), 2)
# 		cv2.line(image, bottomRight, bottomLeft, (0, 255, 0), 2)
# 		cv2.line(image, bottomLeft, topLeft, (0, 255, 0), 2)
# 		# compute and draw the center (x, y)-coordinates of the ArUco
# 		# marker
# 		cX = int((topLeft[0] + bottomRight[0]) / 2.0)
# 		cY = int((topLeft[1] + bottomRight[1]) / 2.0)
# 		cv2.circle(image, (cX, cY), 4, (0, 0, 255), -1)
# 		# draw the ArUco marker ID on the image
# 		cv2.putText(image, str(markerID),
# 			(topLeft[0], topLeft[1] - 15), cv2.FONT_HERSHEY_SIMPLEX,
# 			0.5, (0, 255, 0), 2)
# 		print("[INFO] ArUco marker ID: {}".format(markerID))
# 		# show the output image
# 		cv2.imshow("Image", image)
# 		cv2.waitKey(0)
