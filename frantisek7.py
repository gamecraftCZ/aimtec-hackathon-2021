import cv2
import numpy as np

image = cv2.imread("fanta.png")
arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
arucoDict.bytesList=arucoDict.bytesList[30:,:,:]

arucoParams = cv2.aruco.DetectorParameters_create()
arucoBoard = cv2.aruco.CharucoBoard_create(
    7, 5, .025, .0125, arucoDict
)

(corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
img = cv2.aruco.drawDetectedMarkers(
        image=image,
        corners=corners)

targetId = None
targetCorners = None
corners_all = []
response, charuco_corners, charuco_ids = cv2.aruco.interpolateCornersCharuco(
    markerCorners=corners,
    markerIds=ids,
    image=cv2.cvtColor(img, cv2.COLOR_BGR2GRAY),
    board=arucoBoard)

corners_all.append(charuco_corners)

print(response)

for i, id in enumerate(ids.flatten()):
    targetId = id
    targetCorners = corners[i][0]
    break

if not targetId:
    print("No target id!")


calibration, cameraMatrix, distCoeffs, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco(
    charucoCorners=charuco_corners,
    charucoIds=ids,
    board=arucoBoard,
    imageSize=image.shape[:2],
    cameraMatrix=None,
    distCoeffs=None)

# ret, mtx, dist, rvecs, tvecs = cv2.aruco.calibrateCameraCharuco(corners, ids, arucoBoard, image.shape[:1], camera_matrix, dist_coefs)

# Get bedna offset
re = cv2.aruco.estimatePoseSingleMarkers(targetCorners, 0.05, cameraMatrix, distCoeffs)

pass
