from random import random
from time import sleep

from fanta import getRotDist
from get_front_image import get_front_image
import serial
import cv2

import numpy as np


### PREPARE ARDUINO ###
from top_camera import get_top_camera_all_important

serialArduino = serial.Serial('/dev/ttyACM0', 9600, timeout=0)
sleep(3)


def send2Arduino(str):
    ardbuf = serialArduino.readline()
    try:
        while ardbuf:
            # print("Arduino: " + ardbuf.decode("utf-8"))
            ardbuf = serialArduino.readline()
    except:
        pass
        # print("jahoda")
    print("Pi->Arduino: " + str)
    serialArduino.write(str.encode())
    sleep(0.1)
    ardbuf = serialArduino.readline()
    try:
        while ardbuf:
            # print("Arduino: " + ardbuf.decode("utf-8"))
            ardbuf = serialArduino.readline()
    except:
        pass
        # print("jahoda")



try:
    while ardbuf:
        # print("Arduino: " + ardbuf.decode("utf-8"))
        ardbuf = serialArduino.readline()
except:
    pass
    # print("jahoda")

arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_ARUCO_ORIGINAL)
arucoParams = cv2.aruco.DetectorParameters_create()

for i in range(10):
    send2Arduino("-")
    sleep(0.1)
for i in range(5):
    send2Arduino("+")
    sleep(0.1)


def getOffset(corners, center) -> int:
    (topLeft, topRight, bottomRight, bottomLeft) = corners
    topRight = (int(topRight[0]), int(topRight[1]))
    bottomRight = (int(bottomRight[0]), int(bottomRight[1]))
    bottomLeft = (int(bottomLeft[0]), int(bottomLeft[1]))
    topLeft = (int(topLeft[0]), int(topLeft[1]))
    cY = int((topLeft[0] + bottomRight[0]) / 2.0)
    cX = int((topLeft[1] + bottomRight[1]) / 2.0)

    offset = cY - center
    return offset


BEDNY = [931, 932, 933, 934]
MAPPING = {931: "GREEN", 932: "GREEN", 933: "BLACK", 934: "BLACK"}
try:
    while True:

        #################################################################
        # image = get_front_image()
        # cv2.imshow("Image", image)
        # cv2.waitKey(0)

        ## DRIVE ###
        sleep(1)
        send2Arduino("q")  # Naves dolu
        sleep(8)

        send2Arduino("s")
        sleep(0.3)
        send2Arduino(" ")

        # TODO zahlednout bednu - horní pohled - předelat kód


        image = get_front_image()
        # cv2.imshow("jahoda", image)
        # cv2.waitKey()
        (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
        cv2.aruco.drawDetectedMarkers(image, corners, ids)
        ids = np.array([]) if ids is None else ids
        while not len(set(list(ids.flatten())).intersection(set(BEDNY))):
            if random() > 0.7:
                send2Arduino("w")
                sleep(0.5)
                send2Arduino(" ")
            print("Jahoda")
            send2Arduino("a")
            sleep(0.3)
            send2Arduino(" ")
            sleep(0.5)
            # While we dont see bedna
            image = get_front_image()
            # cv2.imshow("jahoda", image)
            # cv2.waitKey()
            (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
            cv2.aruco.drawDetectedMarkers(image, corners, ids)
            ids = np.array([]) if ids is None else ids


        # Uz vidime bednu
        print("We see bedna!")
        send2Arduino(" ")
        sleep(1)

        # cv2.imshow("bedna", image)
        # cv2.waitKey(0)

        # Detekujeme kde bedna je
        lastTargetId = None
        while True:
            send2Arduino("w")
            sleep(0.5)
            send2Arduino(" ")

            image = get_front_image()
            (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
            targetId = None
            targetCorners = None
            if ids is not None:
                for i, id in enumerate(ids.flatten()):
                    if id in BEDNY:
                        targetId = id
                        targetCorners = corners[i][0]
                        break

            if not targetId:
                print("No target id!")
                break

            lastTargetId = targetId

            # Get bedna offset
            # print("Bedna")
            # while True:
            #     rot, dist = getRotDist(targetCorners)
            #
            #     image = get_front_image()
            #     (corners, ids, rejected) = cv2.aruco.detectMarkers(image, arucoDict, parameters=arucoParams)
            #     targetId = None
            #     targetCorners = None
            #     if ids is not None:
            #         for i, id in enumerate(ids.flatten()):
            #             if id in BEDNY:
            #                 targetId = id
            #                 targetCorners = corners[i][0]
            #                 break
            #
            #     if not targetId:
            #         print("No target id!")
            #         break
            #
            #     if rot > 1.7:
            #         send2Arduino("a")
            #         sleep(0.3)
            #         send2Arduino("w")
            #         sleep(0.1)
            #         send2Arduino("d")
            #         sleep(0.3)
            #         send2Arduino(" ")
            #         sleep(0.2)
            #     else:
            #         send2Arduino(" ")
            #         break

            # Drive to bedna
            print("Drive to bedna")
            center = image.shape[1] // 2

            offset = getOffset(targetCorners, center)
            print("Offset: ", offset)

            if abs(offset) > 25:
                print("new offset")
                if offset < 0:
                    send2Arduino("a")
                elif offset > 0:
                    send2Arduino("d")

                sleep(abs(offset / 5000))
                send2Arduino(" ")

        # Take the bedna
        print("Take the bedna")

        send2Arduino("w")
        sleep(1)
        send2Arduino(" ")
        sleep(1)
        send2Arduino("e")
        sleep(4)
        send2Arduino("s")
        sleep(2)
        send2Arduino(" ")

        # # TODO Jeď - horní pohled
        if lastTargetId:
            newImg, black, green, robot = get_top_camera_all_important()

            centerRobot = [robot[0], robot[1]]
            frontRobot = robot[2]

            greenCenter = (int((green[0][0] + green[1][0]) / 2), int((green[0][1] + green[1][1]) / 2))
            blackCenter = (int((black[0][0] + black[1][0]) / 2), int((black[0][1] + black[1][1]) / 2))

            cv2.circle(newImg, (greenCenter[0], greenCenter[1]), 8, (0, 0, 255), -1)
            cv2.circle(newImg, (blackCenter[0], blackCenter[1]), 8, (0, 0, 255), -1)

            cv2.imshow("NEXT", newImg)
            cv2.waitKey(20)

            if MAPPING[lastTargetId] == "GREEN":
                target = greenCenter
            else:
                target = blackCenter

            vectorRobot = ((target[0] - centerRobot[0]), (target[1] - centerRobot[1]))
            vectorTarget = ((target[0] - centerRobot[0]), (target[1] - centerRobot[1]))

            jahoda = ((vectorRobot[0] / vectorRobot[1]), (vectorTarget[0] / vectorTarget[1]))
            if jahoda[1] < jahoda[0] < jahoda[1] + 0.2:
                send2Arduino("w")
                sleep(1)
                send2Arduino(" ")
                sleep(0.3)
            else:
                send2Arduino("d")
                sleep(0.5)
                send2Arduino(" ")
                sleep(0.3)


finally:
    send2Arduino(" ")
    print("DONE")
