import albumentations as A
import cv2

# https://albumentations.ai/docs/getting_started/bounding_boxes_augmentation/

transform = A.Compose([
    # A.RandomCrop(width=450, height=450),
    A.HorizontalFlip(p=0.5),
    A.RandomBrightnessContrast(p=0.2),
    A.MotionBlur(3, p=0.2),
    A.RGBShift(),
    A.RandomFog(),
    A.RandomGamma(),
    A.RandomShadow(),
    A.RandomSunFlare(),
], bbox_params=A.BboxParams(format='pascal_voc', min_visibility=0.8))

